import numpy as np, pandas as pd, matplotlib.pyplot as plt
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import AdaBoostClassifier, BaggingClassifier

def binarize_continious_data(data):
    #This fuction I create multiple ranges for all continious variables and each age to that group. 
    #After doing this I drop the existing column
    age_group = []
    for age in data["age"]:
        if age < 25:
            age_group.append("<25")
        elif 25 <= age <= 34:
            age_group.append("25-34")
        elif 34 < age <= 44:
            age_group.append("35-44")
        elif 44 < age <= 54:
            age_group.append("45-54")
        elif 54 < age <= 65:
            age_group.append("55-64")
        else:
            age_group.append("65 and over")

    income_df = data.copy()
    income_df["age_group"] = age_group
    del income_df["age"]
    
    workhours_group = []
    for workhours in income_df["workhours"]:
        if workhours < 25:
            workhours_group.append("<25")
        elif 26 <= workhours <= 50:
            workhours_group.append("26-50")
        elif 50 < workhours <= 75:
            workhours_group.append("51-75")
        else:
            workhours_group.append("76 and over")

    new_income_df = income_df.copy()
    new_income_df["workhours_group"] = workhours_group
    del new_income_df["workhours"]
    
    return new_income_df

def data_preprocessing(data):
    #Here I binarize all the continious and catagorical data. As I am binarizing I don't need a scalar fit.
    #I also return the values of x_train and y_train.
    #Also converting the 0 or 1 class in y_train to -1 or 1.
    data = binarize_continious_data(data)
    data = pd.get_dummies(data, drop_first=True)
    return data.loc[:, data.columns != 'income_ >50K'], data['income_ >50K'].map({0: -1, 1: 1})

def main():
    print('Bagging and Boosting output: ')
    cols = ["age", "workclass", "education", "relationship", "profession", "race", "gender", "workhours", "nationality", "income"]
    dev_data = pd.read_csv('income.dev.txt', header = None, names = cols)
    test_data = pd.read_csv('income.test.txt', header = None, names = cols)
    train_data = pd.read_csv('income.train.txt', header = None, names = cols)

    #Doing the data preprocessing over the given data
    data = pd.concat([train_data, dev_data, test_data])
    X, Y = data_preprocessing(data)

    x_train, y_train = X[:len(train_data)], Y[:len(train_data)]
    x_dev, y_dev = X[len(train_data):len(dev_data)+len(train_data)], Y[len(train_data):len(dev_data)+len(train_data)]
    x_test, y_test = X[len(dev_data)+len(train_data):], Y[len(dev_data)+len(train_data):]

    #Run both classifires one after the other
    count = 0
    for classifire in [BaggingClassifier, AdaBoostClassifier]:
        
        cols = ["Depth", "Iterations", "Accuracy"]
        train_accuracy = pd.DataFrame(columns=cols)
        dev_accuracy = pd.DataFrame(columns=cols)
        test_accuracy = pd.DataFrame(columns=cols)
        
        plt.style.use('ggplot')
        fig, axs = plt.subplots(1, 3, figsize=(15, 5), dpi=300, facecolor='w', edgecolor='k', sharex=True)
        
        for max_depth in [1, 2, 3, 5, 10]:

            if count and max_depth > 3:
                break

            for n_estimators in [10, 20, 40, 60, 80, 100]:
                
                #Call the each classifire as required and fit it using the training data
                if not count:
                    bdt = classifire(DecisionTreeClassifier(max_depth=max_depth), n_estimators=n_estimators)
                else:
                    bdt = classifire(DecisionTreeClassifier(max_depth=max_depth), algorithm="SAMME", n_estimators=n_estimators)
                bdt.fit(x_train, y_train)
                
                #Predict and compute accuracy to be plotted
                train_accuracy = train_accuracy.append(
                    {'Depth': max_depth, 
                     'Iterations': n_estimators, 
                     'Accuracy': bdt.score(x_train, y_train)}, 
                    ignore_index=True)
                dev_accuracy = dev_accuracy.append(
                    {'Depth': max_depth, 
                     'Iterations': n_estimators, 
                     'Accuracy': bdt.score(x_dev, y_dev)}, 
                    ignore_index=True)
                test_accuracy = test_accuracy.append(
                    {'Depth': max_depth, 
                     'Iterations': n_estimators, 
                     'Accuracy': bdt.score(x_train, y_train)}, 
                    ignore_index=True)
            
            #Plot as a subplot each train, dev and test data. Everything below this comment is to make
            #the graph pretty.
            axs[0].plot(train_accuracy[train_accuracy["Depth"] == max_depth]["Iterations"], 
                train_accuracy[train_accuracy["Depth"] == max_depth]["Accuracy"], 
                label="Depth "+str(max_depth))
            axs[1].plot(train_accuracy[dev_accuracy["Depth"] == max_depth]["Iterations"], 
                dev_accuracy[dev_accuracy["Depth"] == max_depth]["Accuracy"], 
                label="Depth "+str(max_depth))
            axs[2].plot(train_accuracy[test_accuracy["Depth"] == max_depth]["Iterations"], 
                test_accuracy[test_accuracy["Depth"] == max_depth]["Accuracy"], 
                label="Depth "+str(max_depth))
            
            axs[0].set_title('Training')
            axs[1].set_title('Development')
            axs[2].set_title('Testing')

        for i in range(3):
            if not count:
                fig.suptitle('Accuracy based on depth of tree and number of trees with bagging')
                axs[i].set_xlabel('Number of Trees')
            else:
                fig.suptitle('Accuracy based on depth of tree and iterations of boosting')
                axs[i].set_xlabel('Number of Iterations')

            axs[i].set_ylabel('Accuracy')
            axs[i].legend()

        plt.savefig(str(classifire)+'.png')
        
        if not count:
            print('Training accuracy for Bagging:')
            print(train_accuracy)
            print('Dev accuracy for Bagging:')
            print(dev_accuracy)
            print('Test accuracy for Bagging:')
            print(test_accuracy)
        else:
            print('Training accuracy for boosting:')
            print(train_accuracy)
            print('Dev accuracy for boosting:')
            print(dev_accuracy)
            print('Test accuracy for boosting:')
            print(test_accuracy)

        count += 1

if __name__ == "__main__": main()